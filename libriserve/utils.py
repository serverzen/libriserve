import mimetypes
import sys
import os.path
import logging
import yaml

from pyramid.decorator import reify

logger = logging.getLogger('libriserve')


class AttrDict(object):
    def __getitem__(self, key):
        if not hasattr(self, key):
            raise KeyError(key)
        return getattr(self, key)

EXT_TO_MIMETYPE = {
    'epub': 'application/epub+zip',
}

MIMETYPE_TO_EXT = {}
for k, v in EXT_TO_MIMETYPE.items():
    MIMETYPE_TO_EXT[v] = k


def get_mimetype(ext):
    mimetype = EXT_TO_MIMETYPE.get(ext)
    if not mimetype:
        mimetype, encoding = mimetypes.guess_type('foo.%s' % ext)
    if not mimetype:
        mimetype = 'application/octet-stream'
    return mimetype


def get_format(filename, mimetype=None):
    format = None
    if mimetype:
        format = MIMETYPE_TO_EXT.get(mimetype)
    if not format:
        format = filename.split('.')[-1]
    return format


class Simple(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class LibriServeError(Exception):
    pass


class SettingsLoader(dict):
    etcfile = os.path.join(sys.prefix, 'etc', 'libriserve.yaml')

    def load(self):
        if os.path.exists(self.etcfile):
            with open(self.etcfile) as f:
                data = yaml.load(f)['general']
                self.update(data)

    def save(self, safe=True):
        if safe and os.path.exists(self.etcfile):
            raise OSError('"%s" already exists' % self.etcfile)
        with open(self.etcfile, 'w') as f:
            yaml.dump({'general': self}, f, default_flow_style=False)
