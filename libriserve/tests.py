import os
import shutil
import unittest
from .testing import TempDirTemplate


class BaseTests(TempDirTemplate):
    def test_load_settings(self):
        from .wsgi import SettingsLoader
        tmpdir = self.get_temp_dir()

        # no file exists
        loader = SettingsLoader()
        etcdir = os.path.join(tmpdir, 'etc')
        loader.etcfile = os.path.join(etcdir, 'libriserve.yaml')
        loader.load()
        settings = dict(loader)
        self.assertEqual(settings, {})

        os.mkdir(etcdir)
        with open(loader.etcfile, 'w') as f:
            f.write('general:\n  book_storage: json\n')
        loader.load()
        settings = dict(loader)
        self.assertEqual(settings, {'book_storage': 'json'})


class ViewTests(unittest.TestCase):

    def test_root(self):
        from .views import root
        self.assertTrue('has_permission' in root(None))

    def test_get_download_url(self):
        from .utils import Simple
        from .views import get_download_url
        res = get_download_url(Simple(application_url='http://foobar.com/'),
                               Simple(book_id=456,
                                      id=123))
        self.assertEqual(res, 'http://foobar.com//book_download/456/123')

    def test_get_format(self):
        from .utils import Simple
        from .views import get_bookfile_format
        res = get_bookfile_format(Simple(mimetype='application/epub+zip',
                                         name='foobar.epub'))
        self.assertEqual(res, 'epub')

    def test_book_as_dict(self):
        from .utils import Simple
        from .views import book_as_dict

        res = book_as_dict(Simple(), Simple(title='Foo Bar',
                                            id=1,
                                            files=[],
                                            authors=[]))
        self.assertEqual(res, {'downloads': [],
                               'formats': [],
                               'authors': [],
                               'id': 1,
                               'title': 'Foo Bar'})


class UtilsTests(TempDirTemplate):
    def test_it(self):
        from .utils import SettingsLoader

        loader = SettingsLoader()
        etcdir = os.path.join(self.get_temp_dir(), 'etc')
        loader.etcfile = os.path.join(etcdir, 'libriserve.yaml')
        os.mkdir(etcdir)
        loader['foo'] = 'bar'
        self.assertEqual(loader['foo'], 'bar')
        loader.save()
