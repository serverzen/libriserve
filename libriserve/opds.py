from xml.dom import minidom

_base = u'''<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <id>urn:uuid:%(uuid)s</id>
  <link rel="self"
        href="%(feed_url)s"
        type="application/atom+xml;profile=opds-catalog"/>
  <link rel="start"
        href="%(feed_url)s"
        type="application/atom+xml;profile=opds-catalog"/>
  <title>LibriServe Catalog</title>
</feed>
'''


def _book_node(dom, uuid, book):
    node = dom.createElement('entry')

    title = dom.createElement('title')
    node.appendChild(title)
    title.appendChild(dom.createTextNode(book.title))

    id = dom.createElement('id')
    node.appendChild(id)
    id.appendChild(dom.createTextNode('urn:uuid:%s-%s' % (uuid, str(book.id))))

    for author in book.authors:
        n = dom.createElement('author')
        node.appendChild(n)
        name = dom.createElement('name')
        n.appendChild(name)
        name.appendChild(dom.createTextNode(author.name))

    return node


def get_opds_feed(request):
    books = request.root.content.books

    uuid = request.registry.settings.get('libriserve_uuid')
    if not uuid:
        import uuid
        uuid = str(uuid.uuid4())

    dom = minidom.parseString(_base % {
        'feed_url': request.application_url + '/catalog.atom',
        'uuid': uuid,
    })
    root = dom.documentElement
    for book in books:
        node = _book_node(dom, uuid, book)
        root.appendChild(node)

    return dom.toxml()
