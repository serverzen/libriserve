import unittest
from .testing import TempDirTemplate
from .testing import StorageIntegrationTemplate

HAS_CALIBRE = True
try:
    from .storage import calibrestorage
except ImportError:
    HAS_CALIBRE = False

from .storage import STORAGE_MAP


class SQLIntegrationTests(StorageIntegrationTemplate, unittest.TestCase):
    storage_info = STORAGE_MAP['LibriServe/sql']
    storage_settings = {}
    update_settings = []


class JSONIntegrationTests(StorageIntegrationTemplate, unittest.TestCase):
    storage_info = STORAGE_MAP['LibriServe/json']
    storage_settings = {'libriserve.jsonfile': '%s/test.json'}
    update_settings = ['libriserve.jsonfile']

if HAS_CALIBRE:
    class CalibreIntegrationTests(StorageIntegrationTemplate,
                                  unittest.TestCase):
        storage_info = STORAGE_MAP['LibriServe/calibre']
        storage_settings = {'calibre.library': '%s/calibre'}
        update_settings = ['calibre.library']

    class CalibreTests(TempDirTemplate, unittest.TestCase):
        def test_get_library(self):
            from .utils import Simple

            settings = {'calibre.library': self.get_temp_dir()}
            lib = calibrestorage.get_library(
                Simple(registry=Simple(settings=settings)))
            self.assertNotEqual(lib, None)

        def test_get_file_info(self):
            from .utils import Simple

            info = calibrestorage.get_file_info(
                Simple(title='Foo Bar', id=1), 'foobar.epub')
            self.assertEqual(info.id, 'foobar.epub')
            self.assertEqual(info.mimetype, 'application/epub+zip')
            self.assertEqual(info.name, 'Foo Bar.epub')
            self.assertEqual(info.book_id, 1)

    class CalibreStorageBookModelTests(unittest.TestCase):
        def test_cover_url(self):
            calibrestorage.BookModel(
                request=None, parent=None, id=None, title=None,
                authors=None, available_formats=None)
