require([global_config.jquery_url,
         global_config.knockout_url,
         global_config.bootstrap_js_url,
        'books'],
function($, ko, unused, books) {
    var store = new books.BookStore({
        url: 'c/books/'
    });

    $(store)
        .on('query_begin', function(event, data) {
            book_manager.loading(true);
            book_manager.refresh_class('icon-refresh loading');
        })
        .on('query_success', function(event, data) {
            var books = data.books;
            book_manager.books.removeAll();
            $(books).each(function(idx, book) {
                setTimeout(function() {
                    var book_model = new BookModel(book);
                    book_manager.books.push(book_model);
                }, idx * 50);
            });

            setTimeout(function() {
                book_manager.loading(false);
                book_manager.refresh_class('icon-refresh');
            }, books.length * 50);
        })
        .on('add_success', function(event, book_data) {
            var book = new BookModel(book_data);
            book_manager.books.push(book);
        })
        .on('remove_success', function(event, id) {
            book_manager.books.remove(function(book) {
                return book.id() == id;
            })
        });

    $('#add-book-modal').on('shown', function() {
        $(this).find('input[name="title"]').select();
    });
    $('#confirm-modal').on('shown', function() {
        $(this).find('button.ok-btn').select();
    });

    var BookModel = function(kwargs) {
        var self = this;
        self.title = ko.observable(kwargs.title);
        self.id = ko.observable(kwargs.id);
        self.authors = ko.observableArray();
        self.author_str = ko.computed(function() {
            var s = '';
            $(self.authors()).each(function(idx, author) {
                s += author.name + ', ';
            });
            if (s.length > 0)
                s = s.substring(0, s.length-2);
            return s;
        });

        if (kwargs.authors) {
            $(kwargs.authors).each(function(idx, author) {
                self.authors.push(author);
            });
        }

        self.downloads = ko.observableArray();
        if (kwargs.downloads) {
            for (var x = 0; x < kwargs.downloads.length; x++) {
                var format = kwargs.downloads[x];
                self.downloads.push({
                    format: format.format,
                    download_url: format.url
                });
            }
        }

        self.visit = function() {
            window.location.pathname += 'c/books/' + self.id()
        };
    };

    var confirm = function(kwargs) {
        $('#confirm-modal h3').text(kwargs.title);
        $('#confirm-modal .details').text(kwargs.message);
        
        if (kwargs.ok_text)
            $('#confirm-modal .ok-btn').text(kwargs.ok_text);
        if (kwargs.cancel_text)
            $('#confirm-modal .cancel-btn').text(kwargs.cancel_text);

        if (kwargs.on_ok != null) {
            $('#confirm-modal .ok-btn').click(kwargs.on_ok)
        }

        $('#confirm-modal').modal();
    };


    function strip(s) {
        return s.replace(/^\s+|\s+$/g, '');
    }

    var BookManager = function() {
        var self = this;
        self.loading = ko.observable(true);
        self.books = ko.observableArray();
        self.empty = ko.computed(function() {
            return !self.loading() && self.books().length === 0;
        });

        self.toggle_all = function() {
            var should_check = $('#toggle-checkbox').prop('checked');
            $('.book-listing input[type="checkbox"].selected')
                .prop('checked', should_check);
            return true;
        };

        self.add_book = function() {
            var title = $('input[name="title"]').val();
            var author_str = $('input[name="authors"]').val();
            var s = author_str.split(',');
            var authors = [];
            for (var x = 0; x < s.length; x++) {
                authors.push(strip(s[x]));
            }

            store.add({title: title, authors: authors}, {
                success: function(book) {
                    $('#upload-form input[name="book_id"]').val(book.id);
                    book_manager.start_upload();
                    $('#upload-form').submit();
                }});
        };

        self.show_selection = function() {
            $('.select').removeClass('hide');
            $('#delete-btn').removeAttr('disabled');
        };

        self.hide_selection = function() {
            $('.select').addClass('hide');
            $('#delete-btn').attr('disabled', 'disabled');
        };

        self.toggle_show_selection = function() {
            if ($('.select').hasClass('hide'))
                self.show_selection();
            else
                self.hide_selection();
        };

        self.refresh_class = ko.observable('icon-refresh');

        self.refresh = function() {
            store.query();
        };

        self.delete_books = function() {
            $('.book-listing input:checked.selected').each(function(idx, input) {
                if (input.value !== null && input.value != '')
                    store.remove(parseInt(input.value));
            });
        };

        self.show_delete = function() {
            confirm({
                title: 'Delete?',
                message: 'Are you sure you want to delete the selected items?',
                ok_text: 'Delete',
                on_ok: self.delete_books
            });
        };

        self.show_add_book = function() {
            self.hide_selection();
            $('#add-book-modal').modal();
        };
        self.start_upload = function() {
            $('#add-book-modal loading').addClass('loading');
            $('#add-book-modal .alert')
                .addClass('hide')
                .removeClass('alert-success')
                .removeClass('alert-error');
            return true;
        };
        self.update_book = function(data) {
            for (var x = 0; x < book_manager.books().length; x++) {
                var book = book_manager.books()[x];
                if (book.id() == data.id) {
                    var updated_book = new BookModel(data);
                    book_manager.books.splice(x, 1, updated_book);;
                    break;
                }
            }
        };
        self.finish_upload = function(res) {
            $('#add-book-modal loading').removeClass('loading');
            if (res.success) {
                var m = res.book;
                self.update_book(m);

                $('#add-book-modal .alert')
                    .removeClass('hide')
                    .addClass('alert-success')
                    .text('Book added; Add another?');
            } else {
                $('#add-book-modal .alert')
                    .removeClass('hide')
                    .addClass('alert-error')
                    .text('Error while trying to add book');
            }
            $('#add-book-model input[name="title"]', window.top.document).select();
            return true;
        };
    };

    var book_manager = new BookManager();
    window.libriserve = {
        book_manager: book_manager
    };
    ko.applyBindings(book_manager);

    $('.initial-hide').removeClass('initial-hide');

    store.query();
});
