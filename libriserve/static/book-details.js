define([global_config.jquery_url,
        global_config.knockout_url,
        'books',
        'utils',
        'goog!search,1',
        global_config.bootstrap_js_url],
function($, ko, books, utils) {
    var store = new books.BookStore({url: '../books/'});

    var SearchResultModel = function(base, kwargs) {
        var self = this;

        self.base = base;

        self.title = ko.observable(kwargs.title);
        self.authors = ko.observableArray(kwargs.authors);
        self.cover_url = ko.observable(kwargs.cover_url);
        self.year = ko.observable(kwargs.year);

        self.update_with_google = function() {
            store.update({
                id: self.base.id(),
                title: self.title(),
                authors: self.authors(),
                cover_url: self.cover_url(),
                year: self.year()
            }, {'success': function(res) {
                self.base.title(res.title);
                $(res.authors).each(function(idx, authordata) {
                    var author = new AuthorModel(self, authordata);
                    self.base.authors.push(author);
                });
                $('#info-lookup').modal('hide');
            }});
        };
    };

    var AuthorModel = function(parent, kwargs) {
        var self = this;
        self.parent = parent;

        self.id = ko.observable(kwargs.id);
        self.name = ko.observable(kwargs.name);

        self.remove = function() {
            var idx = self.parent.authors.indexOf(self);
            self.parent.authors.splice(idx, 1);
        };
    };

    var BookModel = function(book_data) {
        var self = this;
        self.id = ko.observable(book_data.id);
        self.title = ko.observable(book_data.title);
        self.authors = ko.observableArray();
        self.being_modified = ko.observable(false);

        $(book_data.authors).each(function(idx, authordata) {
            var author = new AuthorModel(self, authordata);
            self.authors.push(author);
        });

        self.lookup_info = function() {
            self.items.removeAll();
            $('#info-lookup').modal();
        };

        self.save = function() {
            var authors = [];
            $(self.authors()).each(function(idx, obj) {
                authors.push(obj.name());
            });
            store.update({
                id: self.id(),
                title: self.title(),
                authors: authors
            }, { success: function() {
                utils.flash({text: 'Saved'});
                self.set_modifying(false);
            }});
        };

        self.set_modifying = function(flag) {
            if (!flag) {
                $('.btn-save').addClass('hide');
                $('.btn-cancel').addClass('hide');
                $('.btn-modify').removeClass('hide');

                $('.modify').addClass('hide');
                $('.field').removeClass('hide');
            } else {
                $('.btn-save.hide').removeClass('hide');
                $('.btn-cancel.hide').removeClass('hide');
                $('.btn-modify').addClass('hide');

                $('.modify').removeClass('hide');
                $('.field').addClass('hide');
            }
        };

        self.cancel = function() {
            self.set_modifying(false);
        };

        self.modify = function() {
            self.set_modifying(true);
        };

        self.remove = function() {
            utils.confirm({
                title: 'Delete?',
                message: 'Are you sure you want to delete this book?',
                ok_text: 'Delete',
                on_ok: function() {
                    store.remove(self.id());
                    window.location.pathname = '/';
                }
            });
        };

        self.items = ko.observableArray();

        self.search_finished = function() {
            $(self.book_search.results).each(function(idx, item) {
                var cover_url = $(item.html).find('img.gs-image').attr('src');
                var s = new SearchResultModel(self, {
                    title: item.title,
                    authors: item.authors.split(','),
                    cover_url: cover_url,
                    year: item.year
                });
                self.items.push(s);
            });
        };

        self.book_search = new google.search.BookSearch();
        self.book_search.setSearchCompleteCallback(
            self,
            function() {
                self.search_finished();
            }, null);

        self.search = function() {
            self.items.removeAll();
            var v = $('input[name="title"]').val();
            self.book_search.execute(v);
        };
    };

    function init(book_data) {
        $('dd.author.hide').removeClass('hide');
        ko.applyBindings(new BookModel(book_data));
        $('dd.author.provided').remove();
    }

    return {init: init};
});
