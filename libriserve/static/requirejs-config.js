requirejs_config = {
    shim: {},
    baseUrl: global_config.base_url,
    paths: {
        async: 'third-party/async',
        goog: 'third-party/goog',
        propertyParser: 'third-party/propertyParser',
    }
};
requirejs_config.shim[global_config.jquery_url] = {
    exports: '$'
};
requirejs_config.shim[global_config.bootstrap_js_url] = {
    deps: [global_config.jquery_url],
    exports: '$'
};

requirejs.config(requirejs_config);
