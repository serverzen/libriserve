define([global_config.jquery_url],
function($) {
    var BookStore = function(store_kwargs) {
        var self = this;
        var url = store_kwargs.url;

        self.update = function(book_data, options) {
            var options = options || {};
            var req = $.ajax(url + book_data.id, {
                type: 'PUT',
                data: JSON.stringify(book_data)
            });
            req.success(function(res) {
                $(self).trigger('update_success', [res]);
                if (options.success)
                    options.success(res);
            });
        };

        self.add = function(book_data, options) {
            var options = options || {};

            var req = $.ajax(url, {
                type: 'POST',
                data: JSON.stringify(book_data)
            });

            req.success(function(res) {
                $(self).trigger('add_success', [res]);
                if (options.success)
                    options.success(res);
            });
            
        };
        self.remove = function(id) {
            var req = $.ajax(url + id, {type: 'DELETE'});
            req.success(function(res) {
                $(self).trigger('remove_success', [id]);
            });
        };
        self.query = function() {
            $(self).trigger('query_begin', []);
            var res = $.getJSON(url);
            res.success(function(data) {
                $(self).trigger('query_success', [data]);
            });
        };
    };
    
    return {BookStore: BookStore};
});
