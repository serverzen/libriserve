define([global_config.jquery_url],
function($) {
    function flash(kwargs) {
        var mtype = kwargs.type || 'info';
        var html = '<div class="alert alert-'+mtype+'">'+kwargs.text+'</div>';
        $('#main').prepend(html);
    }

    var confirm = function(kwargs) {
        console.log($('#confirm-modal').length);
        if ($('#confirm-modal').length == 0) {
            $('body').append('<div class="hide modal fade" id="confirm-modal">');
            var div = $('#confirm-modal');
            div.append('<div class="modal-header">');
            var header = $('#confirm-modal .modal-header');
            header.append('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>');
            header.append('<h3>');
            div.append('<div class="modal-body">');
            var body = $('#confirm-modal .modal-body');
            body.append('<p class="details"></p>');
            div.append('<div class="modal-footer">');
            var footer = $('#confirm-modal .modal-footer');
            footer.append('<button class="btn btn-primary ok-btn" data-dismiss="modal" aria-hidden="true">Ok</button>');
            footer.append('<button class="btn cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>');
        }

        $('#confirm-modal h3').text(kwargs.title);
        $('#confirm-modal .details').text(kwargs.message);
        
        if (kwargs.ok_text)
            $('#confirm-modal .ok-btn').text(kwargs.ok_text);
        if (kwargs.cancel_text)
            $('#confirm-modal .cancel-btn').text(kwargs.cancel_text);

        if (kwargs.on_ok != null) {
            $('#confirm-modal .ok-btn').click(kwargs.on_ok)
        }

        $('#confirm-modal').modal();
    };

    return {flash: flash, confirm: confirm};
});
