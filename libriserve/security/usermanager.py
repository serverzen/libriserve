from .sql import User


class UserManager(object):
    def __init__(self, settings):
        self.settings = settings

    def add(self, username, password, email):
        db = self.settings['db.sessionmaker']()
        user = User(user_name=username,
                    email=email,
                    status=1)
        user.set_password(password)
        db.add(user)
        db.commit()
