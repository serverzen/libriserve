from pyramid.security import unauthenticated_userid
from ziggurat_foundations import ziggurat_model_init

from .sql import (User, Group, UserGroup, GroupPermission, UserPermission,
                  UserResourcePermission, GroupResourcePermission, Resource,
                  ExternalIdentity)

from .views import login, login_post, logout
from .usermanager import UserManager


def includeme(config):
    config.add_route('login', '/auth/login',
                     request_method='GET')
    config.add_route('logout', '/auth/logout',
                     request_method='GET')
    config.add_route('login_post', '/auth/login',
                     request_method='POST')

    config.add_view(logout, route_name='logout')
    config.add_view(login, route_name='login',
                    renderer='libriserve.security:templates/login.jinja2')
    config.add_view(login_post, route_name='login_post',
                    renderer='libriserve.security:templates/login.jinja2')
    config.set_request_property(get_user, 'user', reify=True)

    usermanager = UserManager(config.registry.settings)
    config.registry.settings['security.usermanager'] = usermanager


class AnonymousUser(object):
    user_name = 'anonymous'
    permissions = []
    is_anonymous = True

anonymous = AnonymousUser()


def get_user(request):
    # the below line is just an example, use your own method of
    # accessing a database connection here (this could even be another
    # request property such as request.db, implemented using this same
    # pattern).
    userid = unauthenticated_userid(request)
    if userid is not None:
        # this should return None if the user doesn't exist
        # in the database
        return request.db.query(User).filter(User.id == userid).first() \
            or anonymous
    return anonymous


ziggurat_model_init(User, Group, UserGroup, GroupPermission, UserPermission,
                    UserResourcePermission, GroupResourcePermission, Resource,
                    ExternalIdentity, passwordmanager=None)
