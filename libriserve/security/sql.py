from ziggurat_foundations.models import UserMixin, GroupMixin
from ziggurat_foundations.models import GroupPermissionMixin, UserGroupMixin
from ziggurat_foundations.models import (GroupResourcePermissionMixin,
                                         ResourceMixin)
from ziggurat_foundations.models import (UserPermissionMixin,
                                         UserResourcePermissionMixin)
from ziggurat_foundations.models import ExternalIdentityMixin

from ..storage.sqlstorage import Base


# Base is sqlalchemy's Base = declarative_base() from your project
class Group(GroupMixin, Base):
    pass


class GroupPermission(GroupPermissionMixin, Base):
    pass


class UserGroup(UserGroupMixin, Base):
    pass


class GroupResourcePermission(GroupResourcePermissionMixin, Base):
    pass


class Resource(ResourceMixin, Base):
    pass


class UserPermission(UserPermissionMixin, Base):
    pass


class UserResourcePermission(UserResourcePermissionMixin, Base):
    pass


class User(UserMixin, Base):
    is_anonymous = False


class ExternalIdentity(ExternalIdentityMixin, Base):
    pass
