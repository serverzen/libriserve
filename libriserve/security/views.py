from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember, forget

from .sql import User


def login(request):
    return {}


def logout(request):
    headers = forget(request)
    return HTTPFound(request.application_url, headers=headers)


def login_post(request):
    username = request.POST['username']
    password = request.POST['password']
    user = request.db.query(User).filter(User.user_name == username).one()
    if user.check_password(password):
        headers = remember(request, user.id, max_age='86400')
        return HTTPFound(request.application_url, headers=headers)
    return {
        'success': False,
        'message': 'Invalid username/password'
    }
