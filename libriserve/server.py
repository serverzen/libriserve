import logging
import argparse
import os.path
import sys

from wsgiref.simple_server import make_server
from . import app
from .storage import STORAGES, STORAGE_GROUPS

logger = logging.getLogger('libriserve')


class HelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    def format_help(self):
        storage_help = '\nAvailable Storages:\n\n'
        for group, storages in STORAGE_GROUPS.items():
            storage_help += '  [%s]\n' % group
            for storage in storages:
                factory = storage.factory
                storage_help += '    Name: %s\n' % storage.name
                storage_help += '    Options:\n'
                for setting_name, help, default in factory.setting_options:
                    storage_help += '      %-15s  %s (default: %s)\n' \
                        % (setting_name, help, default)

            storage_help += '\n'
        s = super(argparse.ArgumentDefaultsHelpFormatter, self).format_help()
        return s + storage_help + '\n'


def main(args=None, global_config=None):
    if args is None:
        prog = 'script'
        args = []
    else:
        prog = os.path.basename(args[0])
        args = args[1:]

    storages_str = ', '.join([storage.name for storage in STORAGES])

    storage_help = 'Available Storages:\n\n'
    for storage in STORAGES:
        storage_help += '  %8s\n' % storage.name

    parser = argparse.ArgumentParser(
        description='Run the content server',
        prog=prog,
        formatter_class=HelpFormatter)
    parser.add_argument('--host', dest='host', default='0.0.0.0',
                        help='Host/IP to listen on '
                             '[0.0.0.0 means all interfaces]')
    parser.add_argument('--port', dest='port', default=8080,
                        type=int,
                        help='Port to listen on')
    parser.add_argument('--storage', dest='book_storage', default='sql',
                        help='Storage type to use [options: %s]'
                             % storages_str)
    parser.add_argument('--storage-settings', dest='storage_settings',
                        help='Configuration settings for storage; settings '
                             'need to be of foo=bar,abc=def format')
    args = parser.parse_args(args)

    additional = {}
    for x in (args.storage_settings or '').split(','):
        if '=' not in x:
            continue
        name, value = x.split('=')
        additional[name.strip()] = value.strip()

    server = make_server(args.host, args.port,
                         app(global_config,
                             book_storage=args.book_storage,
                             **additional))
    logger.info('Listening on: %s:%i' % (args.host, args.port))
    server.serve_forever()


def console():
    logging.basicConfig()
    logger.setLevel(logging.INFO)

    main(sys.argv)


if __name__ == '__main__':
    console()
