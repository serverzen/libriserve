"""empty message

Revision ID: 32e0ed853d78
Revises: None
Create Date: 2013-01-28 17:54:11.880878

"""

# revision identifiers, used by Alembic.
revision = '32e0ed853d78'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('groups',
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('group_name', sa.Unicode(length=128), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('member_count', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('user_password', sa.String(length=256), nullable=True),
    sa.Column('registered_date', sa.TIMESTAMP(), server_default='CURRENT_TIMESTAMP', nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('last_login_date', sa.TIMESTAMP(), server_default='CURRENT_TIMESTAMP', nullable=True),
    sa.Column('status', sa.SmallInteger(), nullable=False),
    sa.Column('user_name', sa.Unicode(length=30), nullable=True),
    sa.Column('email', sa.Unicode(length=100), nullable=False),
    sa.Column('security_code', sa.String(length=256), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('user_name')
    )
    op.create_table('libriserve_authors',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Unicode(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('libriserve_books',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.Unicode(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users_groups',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['groups.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'group_id')
    )
    op.create_table('external_identities',
    sa.Column('access_token', sa.String(length=255), nullable=True),
    sa.Column('alt_token', sa.String(length=255), nullable=True),
    sa.Column('external_user_name', sa.Unicode(length=255), nullable=True),
    sa.Column('local_user_name', sa.Unicode(length=50), nullable=False),
    sa.Column('provider_name', sa.Unicode(length=50), nullable=False),
    sa.Column('external_id', sa.Unicode(length=255), nullable=False),
    sa.Column('token_secret', sa.String(length=255), nullable=True),
    sa.ForeignKeyConstraint(['local_user_name'], ['users.user_name'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('local_user_name', 'provider_name', 'external_id')
    )
    op.create_table('libriserve_bookauthors',
    sa.Column('author_id', sa.Integer(), nullable=True),
    sa.Column('book_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['author_id'], ['libriserve_authors.id'], ),
    sa.ForeignKeyConstraint(['book_id'], ['libriserve_books.id'], ),
    sa.PrimaryKeyConstraint()
    )
    op.create_table('groups_permissions',
    sa.Column('perm_name', sa.Unicode(length=30), nullable=False),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['groups.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('perm_name', 'group_id')
    )
    op.create_table('users_permissions',
    sa.Column('perm_name', sa.Unicode(length=30), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('perm_name', 'user_id')
    )
    op.create_table('resources',
    sa.Column('resource_name', sa.Unicode(length=100), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.Column('owner_user_id', sa.Integer(), nullable=True),
    sa.Column('owner_group_id', sa.Integer(), nullable=True),
    sa.Column('ordering', sa.Integer(), nullable=False),
    sa.Column('parent_id', sa.Integer(), nullable=True),
    sa.Column('resource_type', sa.Unicode(length=30), nullable=False),
    sa.ForeignKeyConstraint(['owner_group_id'], ['groups.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['owner_user_id'], ['users.id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.ForeignKeyConstraint(['parent_id'], ['resources.resource_id'], onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('resource_id')
    )
    op.create_table('libriserve_bookfiles',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('data', sa.LargeBinary(), nullable=True),
    sa.Column('name', sa.Unicode(length=255), nullable=False),
    sa.Column('mimetype', sa.String(length=30), nullable=False),
    sa.Column('book_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['book_id'], ['libriserve_books.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('groups_resources_permissions',
    sa.Column('perm_name', sa.Unicode(length=50), nullable=False),
    sa.Column('resource_id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['groups.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['resource_id'], ['resources.resource_id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('perm_name', 'resource_id', 'group_id')
    )
    op.create_table('users_resources_permissions',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('resource_id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('perm_name', sa.Unicode(length=50), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resources.resource_id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'resource_id', 'perm_name')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users_resources_permissions')
    op.drop_table('groups_resources_permissions')
    op.drop_table('libriserve_bookfiles')
    op.drop_table('resources')
    op.drop_table('users_permissions')
    op.drop_table('groups_permissions')
    op.drop_table('libriserve_bookauthors')
    op.drop_table('external_identities')
    op.drop_table('users_groups')
    op.drop_table('libriserve_books')
    op.drop_table('libriserve_authors')
    op.drop_table('users')
    op.drop_table('groups')
    ### end Alembic commands ###
