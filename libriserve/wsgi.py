import logging
import pkg_resources

from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy

from .models import RootModel
from .dbsetup import init_db, check_db, DatabaseRequiresMigrationError
from .utils import SettingsLoader


logger = logging.getLogger('libriserve')


def dumb_app(global_config=None, **settings):
    """ This function returns a WSGI application.
    """

    if global_config is None:
        global_config = {}

    authentication_policy = AuthTktAuthenticationPolicy('seekrit')
    authorization_policy = ACLAuthorizationPolicy()

    config = Configurator(root_factory=RootModel, settings=settings,
                          authentication_policy=authentication_policy,
                          authorization_policy=authorization_policy)
    config.include('libriserve.dbsetup')
    config.include('libriserve.security')
    config.add_translation_dirs('locale/')
    config.include('pyramid_jinja2')

    config.add_static_view('static', 'static')
    config.scan()

    return config.make_wsgi_app()


def app(global_config=None, **settings):
    """ This function returns a WSGI application.
    """

    if global_config is None:
        global_config = {}

    settings = dict(settings)
    loader = SettingsLoader()
    loader.load()
    settings.update(loader)
    storage_name = settings['book_storage']
    info = pkg_resources.get_entry_info('LibriServe',
                                        'libriserve.storages', storage_name)
    storage = info.load()()
    logger.info('Storage: %s' % storage_name)
    settings['libriserve.storage'] = storage
    init_db(settings=settings)
    storage.init(settings)
    try:
        check_db(settings=settings)
        return dumb_app(global_config, **settings)
    except DatabaseRequiresMigrationError:
        from .failsafe import app
        return app(global_config, **settings)
