from pyramid.config import Configurator

from .views import index


def app(global_config=None, **settings):
    config = Configurator()
    config.include('pyramid_jinja2')
    config.add_route('index', '/')
    config.add_view(index, route_name='index',
                    renderer='libriserve.failsafe:templates/index.jinja2')
    config.add_static_view('static', '../static')
    return config.make_wsgi_app()
