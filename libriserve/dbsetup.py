import os.path
import sys
import logging

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError

from .utils import LibriServeError

logger = logging.getLogger('libriserve')

DEFAULT_SQL_URL = 'sqlite:///libriserve.db'
_etcdir = os.path.join(sys.prefix, 'etc')
if os.path.isdir(_etcdir):
    DEFAULT_SQL_URL = 'sqlite:///%s/libriserve.db' % _etcdir


def init_db(config_or_request=None, settings=None):
    if config_or_request is not None and settings is None:
        settings = config_or_request.registry.settings
    settings.setdefault('sqlalchemy.url', DEFAULT_SQL_URL)
    logger.info('Database URL: %s' % settings['sqlalchemy.url'])
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    maker = sessionmaker(bind=engine)
    settings.setdefault('db.engine', engine)
    settings.setdefault('db.sessionmaker', maker)


def includeme(config):
    init_db(config)
    config.set_request_property(get_db, 'db', reify=True)


def get_db(request):
    return request.registry.settings['db.sessionmaker']()


class DatabaseRequiresMigrationError(LibriServeError):
    def __init__(self):
        super(DatabaseRequiresMigrationError, self).__init__(
            'Database requires migration')


def check_db(config_or_request=None, settings=None):
    from .storage.sqlstorage import Book

    if config_or_request is not None and settings is None:
        settings = config_or_request.registry.settings

    db = settings['db.sessionmaker']()
    try:
        db.query(Book).first()
    except OperationalError:
        raise DatabaseRequiresMigrationError()
