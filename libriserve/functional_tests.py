import tempfile
import unittest
import shutil
from webtest import TestApp
from .utils import Simple


class JSONFunctionalTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        from .wsgi import dumb_app as app_factory
        from .storage.sqlstorage import BookStorage
        from .dbsetup import init_db
        from .manage import upgrade

        cls.tmpdir = tempfile.mkdtemp()

        url = 'sqlite:///%s/libriserve-functional.db' % cls.tmpdir
        settings = {'sqlalchemy.url': url}
        storage = BookStorage()
        settings['libriserve.storage'] = storage
        init_db(settings=settings)
        upgrade(Simple(sql_url=settings['sqlalchemy.url']))
        storage.init(settings)

        app = app_factory({}, **settings)
        cls.testapp = TestApp(app)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.tmpdir)

    def tearDown(self):
        from .dbsetup import get_db

        registry = self.testapp.app.registry
        book_storage = registry.settings['libriserve.storage']
        request = Simple(registry=registry)
        request.db = get_db(request)
        books = book_storage.get_book_container(request)
        allbooks = [x for x in books]
        for x in allbooks:
            del books[x.id]

    def test_book_listing(self):
        res = self.testapp.get('/c/books/', status=200)
        data = res.json_body
        self.assertEqual(data, {'books': []})

    def test_add_delete_book(self):
        # first setup a new user
        settings = self.testapp.app.registry.settings
        usermanager = settings['security.usermanager']
        usermanager.add('foo', 'bar', 'foo@bar.com')

        # log in as new user, successful login will be redirected
        res = self.testapp.post('/auth/login',
                                params={'username': 'foo',
                                        'password': 'bar'},
                                status=302)

        # add a book that automatically creates new authors
        res = self.testapp.post_json(
            '/c/books/',
            params={
                'title': 'Foo Bar',
                'authors': ['First Guy', 'Second Guy'],
            },
            status=200)
        body = res.json_body
        self.assertEqual(body['title'], 'Foo Bar')

        authors = sorted([x['name'] for x in body['authors']])
        self.assertEqual(authors,  ['First Guy', 'Second Guy'])
        author1 = body['authors'][0]

        # add a book that uses existing authors
        res = self.testapp.post_json(
            '/c/books/',
            params={
                'title': 'Foo Bar',
                'authors': [{'id': author1['id']}],
            },
            status=200)
        body = res.json_body
        authors = sorted([x['name'] for x in body['authors']])
        self.assertEqual(authors,  [author1['name']])

        res = self.testapp.delete('/c/books/' + str(body['id']),
                                  status=200)
        self.assertEqual(res.json_body, {'success': True})
