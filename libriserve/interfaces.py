from zope.interface import Attribute
from zope.interface import Interface


class IFile(Interface):
    filename = Attribute('A string representing the filename of this file')
    file = Attribute('An open file object for the data')


class IAuthor(Interface):
    id = Attribute('ID')
    name = Attribute('Author name')


class IBook(Interface):
    id = Attribute('ID')
    title = Attribute('Book title')


class IBookContainer(Interface):
    def __getitem__(key):
        '''Return the book referenced by key'''

    def __iter__():
        ''''''

    def __delitem__(key):
        ''''''

    def add(title, authors):
        '''Add a new book
        '''

    def add_file(book_id, fileobj):
        '''Add a new file for the given book, fileobj will be an instance
        of IFile.
        '''

    def update(id, title, authors):
        '''Update book
        '''


class IBookStorage(Interface):
    setting_options = Attribute('Settings used by this storage')

    def init(settings):
        '''Initialize the storage based on the given settings
        '''

    def get_book_container(request):
        '''Return a book container instance with the given request
        '''

    def get_author_container(request):
        '''Return a book container instance with the given request
        '''

    def get_data_iter(request, download):
        '''Return an iterable of binary data for the given download
        '''

    def get_file_info(request, download):
        '''Return info regarding the download
        '''


class IFile(Interface):
    pass


class IFileContainer(Interface):
    pass
