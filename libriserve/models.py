from pyramid.security import Allow, Authenticated, Everyone
from zope.interface import implements

from .interfaces import IBook, IBookContainer, IFile, IFileContainer

from .utils import reify
from .utils import AttrDict


class ImageModel(object):
    def __init__(self, parent, name):
        self.__parent__ = parent
        self.name = name

    @reify
    def id(self):
        return int(self.name.split('.')[0])

    @reify
    def ext(self):
        return self.name.split('.')[-1]


class DownloadModel(object):
    def __init__(self, parent, id):
        self.__parent__ = parent
        self.id = id


class BookDownloadModel(object):
    def __init__(self, parent, id):
        self.__parent__ = parent
        self.id = id

    def __getitem__(self, key):
        return DownloadModel(self, key)


class DownloadContainerModel(object):
    def __init__(self, parent):
        self.__parent__ = parent

    def __getitem__(self, key):
        return BookDownloadModel(self, key)


class BaseResourceContainer(object):
    def __init__(self, parent, real_container):
        self.__parent__ = parent
        self.__acl__ = parent.__acl__
        self.real_container = real_container

    def __getitem__(self, key):
        return self.resource_factory(self, self.real_container[key])

    def __iter__(self):
        return (self.resource_factory(self, x) for x in self.real_container)

    def add(self, **kw):
        return self.resource_factory(self, self.real_container.add(**kw))

    def __delitem__(self, key):
        del self.real_container[key]

    def update(self, **kw):
        return self.resource_factory(self, self.real_container.update(**kw))


class BaseResource(object):
    def __init__(self, parent, data):
        self.__parent__ = parent
        self._data = data

    def __getattr__(self, key):
        if not key.startswith('_'):
            return getattr(self._data, key)
        return object.__getattr__(self, key)

    def __cmp__(self, other):
        return cmp(self._data, other._data)


class BookModel(BaseResource):
    implements(IBook)


class BookContainerModel(BaseResourceContainer):
    implements(IBookContainer)

    resource_factory = BookModel


class FileModel(BaseResource):
    implements(IFile)


class FileContainerModel(BaseResourceContainer):
    implements(IFileContainer)
    resource_factory = FileModel


class ContentContainerModel(AttrDict):
    def __init__(self, parent, books, authors, files):
        self.__parent__ = parent
        self.__acl__ = parent.__acl__
        self.books = books
        self.authors = authors
        self.files = files


class RootModel(AttrDict):
    __acl__ = [
        (Allow, Everyone, u'view'),
        (Allow, Authenticated, u'add_book'),
        (Allow, Authenticated, u'add_book_file'),
        (Allow, Authenticated, u'delete_book'),
        (Allow, Authenticated, u'modify_book'),
    ]

    def __init__(self, request):
        self._request = request
        if request.user:
            for perm_user, perm_name in request.user.permissions:
                self.__acl__.append((Allow, perm_user, perm_name,))

    @reify
    def c(self):
        book_storage = self._request.registry.settings['libriserve.storage']
        books = book_storage.get_book_container(self._request)
        files = book_storage.get_file_container(self._request)
        authors = book_storage.get_author_container(self._request)
        authors.__acl__ = self.__acl__
        return ContentContainerModel(self,
                                     BookContainerModel(self, books),
                                     authors,
                                     FileContainerModel(self, files))

    @reify
    def book_download(self):
        return DownloadContainerModel(self)
