import argparse
import getpass
import uuid

from .wsgi import SettingsLoader
from .dbsetup import DEFAULT_SQL_URL


def init(args):
    loader = SettingsLoader()
    loader['libriserve_uuid'] = str(uuid.uuid4())
    loader['book_storage'] = 'sql'
    try:
        loader.save()
        upgrade(args)
    except OSError, ex:
        print 'Looks like there is already an environment setup'
        print str(ex)


def adduser(args):
    from .security import User
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    engine = create_engine(args.sql_url)
    db = sessionmaker(bind=engine)()

    user = User(user_name=args.username,
                email=args.email,
                status=1)
    password1 = 1
    password2 = 2
    while password1 != password2:
        password1 = getpass.getpass()
        password2 = getpass.getpass('Password (confirm): ')
    user.set_password(password1)

    db.add(user)
    db.commit()


def upgrade(args):
    from alembic.command import upgrade, stamp
    from alembic.config import Config
    import libriserve
    alembic_cfg = Config()
    alembic_cfg.config_file_name = \
        libriserve.__path__[0] + '/migrations/alembic.ini'
    alembic_cfg.set_main_option("script_location", "libriserve:migrations")
    alembic_cfg.set_main_option("sqlalchemy.url", args.sql_url)
    stamp(alembic_cfg, None)
    upgrade(alembic_cfg, 'head')


def main(argv=None):
    loader = SettingsLoader()
    loader.load()
    settings = dict(loader)
    default_sql_url = DEFAULT_SQL_URL
    if 'general' in settings:
        if 'sqlalchemy.url' in settings['general']:
            default_sql_url = settings['general']['sqlalchemy.url']

    parser = argparse.ArgumentParser(
        description='Run the content server',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_init = subparsers.add_parser(
        'init', help='Initialize database and configuration file')
    parser_init.add_argument(
        '--sql-url',
        help='SQLAchemy-based url for connecting to a database',
        default=default_sql_url)
    parser_init.set_defaults(func=init)

    parser_upgrade = subparsers.add_parser(
        'upgrade', help='Upgrade the database')
    parser_upgrade.add_argument(
        '--sql-url',
        help='SQLAchemy-based url for connecting to a database',
        default=default_sql_url)
    parser_upgrade.set_defaults(func=upgrade)

    parser_adduser = subparsers.add_parser(
        'adduser', help='Add a user')
    parser_adduser.add_argument(
        'username',
        help='User name')
    parser_adduser.add_argument(
        'email',
        help='E-Mail')
    parser_adduser.add_argument(
        '--sql-url',
        help='SQLAchemy-based url for connecting to a database',
        default=default_sql_url)
    parser_adduser.set_defaults(func=adduser)

    args = parser.parse_args(argv)
    args.func(args)


if __name__ == '__main__':
    import sys
    main(sys.argv)
