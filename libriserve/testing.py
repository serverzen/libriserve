import json
import tempfile
import shutil

from .utils import Simple


class TempDirTemplate(object):
    @classmethod
    def get_temp_dir(cls):
        if not hasattr(cls, 'tmpdir'):
            cls.tmpdir = tempfile.mkdtemp()
        #print cls.__name__, cls.tmpdir
        return cls.tmpdir

    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'tmpdir'):
            shutil.rmtree(cls.tmpdir)
            #print cls.__name__, cls.tmpdir
            del cls.tmpdir


class StorageIntegrationTemplate(TempDirTemplate):

    @classmethod
    def setUpClass(cls):
        from .models import RootModel
        from .manage import upgrade
        from .dbsetup import get_db, init_db

        settings = dict(cls.storage_settings)
        settings.setdefault('sqlalchemy.url',
                            'sqlite:///%s/test.db' % cls.get_temp_dir())
        for x in cls.update_settings:
            settings[x] = settings[x] % cls.get_temp_dir()
        init_db(settings=settings)
        storage = cls.storage_info.factory()
        upgrade(Simple(sql_url=settings['sqlalchemy.url']))
        storage.init(settings)
        settings['libriserve.storage'] = storage
        cls.settings = settings
        request = Simple(registry=Simple(settings=settings),
                         user=None)
        cls.db = request.db = get_db(request)
        cls.root_model = RootModel(request)

    def test_basic(self):
        # adding a book
        file_container = self.root_model.c['files']
        book_container = self.root_model.c['books']
        book = book_container.add(title=u'Foo Bar')
        self.assertEqual(book.title, 'Foo Bar')

        # iterating over all books
        books = [x for x in book_container]
        self.assertEqual(len(books), 1)

        # adding a file to a book
        filename = unicode(self.get_temp_dir() + '/binary.epub')
        with open(filename, 'wb') as binfile:
            binfile.write('hello world')
        with open(filename, 'rb') as binfile:
            file_container.add(book_id=book.id, data=binfile,
                               name='binary.epub')

        # retrieving a book by id
        lookedup = book_container[book.id]
        self.assertEqual(lookedup, book)

        # deleting a book
        del book_container[book.id]
        books = [x for x in book_container]
        self.assertEqual(len(books), 0)

    def _add_book(self):
        from .views import add_book

        container = self.root_model['c']['books']
        # add a new book
        res = add_book(Simple(context=container,
                              json_body={'title': u'Foo Bar'}))
        self.book = container[res['id']]
        return res

    def _cleanup_books(self):
        book_ids = [x.id for x in self.root_model['c']['books']]
        for x in book_ids:
            del self.root_model['c']['books'][x]

    def test_view_book_listing(self):
        from .views import books

        container = self.root_model['c']['books']
        self.assertEqual(books(Simple(context=container)), {'books': []})

    def test_view_add_book(self):
        res = self._add_book()
        self.assertEqual(res['title'], 'Foo Bar')
        self._cleanup_books()

    def test_view_get_book(self):
        from .views import book

        container = self.root_model['c']['books']
        res = self._add_book()
        res = book(Simple(context=container[res['id']]))
        book = json.loads(res['book'])
        self.assertEqual(book['title'], 'Foo Bar')

    def test_view_upload_book(self, cleanup=True):
        import json
        from .views import upload

        container = self.root_model['c']['books']
        book_id = self._add_book()['id']
        # upload a book file with no file
        res = upload(Simple(context=self.root_model,
                            POST={}))
        res = json.loads(res['result'])
        self.assertFalse(res['success'])

        # upload a book file with no book_id
        res = upload(Simple(context=self.root_model,
                            POST={'file': Simple()}))
        res = json.loads(res['result'])
        self.assertFalse(res['success'])

        # upload a book correctly
        newfile = self.get_temp_dir() + '/randomfile.epub'
        with open(newfile, 'wb') as f:
            f.write('nothing')
        with open(newfile, 'rb') as f:
            res = upload(Simple(
                root=self.root_model,
                context=self.root_model,
                application_url='http://someplace.com/',
                POST={'file': Simple(filename=u'abc.epub', file=f),
                      'book_id': book_id}))
            res = json.loads(res['result'])
            self.assertTrue(res['success'])

        self.book = container[book_id]

        if cleanup:
            self._cleanup_books()

    def test_download_book(self):
        from .views import download

        self.test_view_upload_book(cleanup=False)

        # try to download non-existent book file
        context = Simple(id=1234, __parent__=Simple(id=self.book.id))
        request = Simple(context=context,
                         db=self.db,
                         root=self.root_model,
                         registry=Simple(settings=self.settings))
        self.assertRaises(KeyError, download, request)

        # download book file properly
        context.id = self.book.files[0].id
        data = download(request)
        testdata = ''
        for x in data.app_iter:
            testdata += x
        self.assertEqual(testdata, 'nothing')

        self._cleanup_books()

    def test_delete_book(self):
        from .views import delete_book

        container = self.root_model['c']['books']
        book_id = self._add_book()['id']
        res = delete_book(Simple(root=self.root_model,
                                 context=container[book_id]))
        self.assertEqual(res, {'success': True})

    def test_update_book(self):
        from .views import save_book

        book_id = self._add_book()['id']
        res = save_book(Simple(
            root=self.root_model,
            json_body={'id': book_id, 'title': u'Changed', 'authors': []}
        ))
        self.assertEqual(res['title'], 'Changed')
