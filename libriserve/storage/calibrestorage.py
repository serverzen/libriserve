import sys
import os

path = os.environ.get('CALIBRE_PYTHON_PATH', '/usr/lib/calibre')
sys.path.insert(0, path)

sys.resources_location = \
    os.environ.get('CALIBRE_RESOURCES_PATH', '/usr/share/calibre')
sys.extensions_location = \
    os.environ.get('CALIBRE_EXTENSIONS_PATH',
                   '/usr/lib/calibre/calibre/plugins')
sys.executables_location = \
    os.environ.get('CALIBRE_EXECUTABLES_PATH', '/usr/bin')


import datetime

from zope.interface import implements
from ..interfaces import IBook, IBookContainer, IBookStorage

from calibre.library.database2 import LibraryDatabase2
from calibre.ebooks.metadata.book.base import Metadata

from ..utils import reify
from ..utils import Simple
from ..utils import get_mimetype


def get_library(request):
    if not hasattr(request, 'libriserve_db'):
        request.libriserve_library = \
            LibraryDatabase2(request.registry.settings['calibre.library'])
    return request.libriserve_library


def get_file_info(book, format):
    ext = format.split('.')[-1]
    mimetype = get_mimetype(ext)
    info = Simple(id=format,
                  mimetype=mimetype,
                  name='%s.%s' % (book.title, ext),
                  book_id=book.id)
    return info


class BookModel(object):
    implements(IBook)

    def __init__(self, request, parent, id, title,
                 authors, **kwargs):
        self._request = request
        self.__parent__ = parent

        self.id = id
        self.title = title
        self.authors = authors
        self.available_formats = kwargs.get('available_formats', [])

        for k, v in kwargs.items():
            if isinstance(v, datetime.datetime):
                v = u'%i-%i-%i' % (v.year, v.month, v.day)
            self.__dict__[k] = v

    @reify
    def files(self):
        formats = []
        for x in getattr(self, 'available_formats', None) or []:
            formats.append(get_file_info(self, x))
        return formats

    def __eq__(self, other):
        return self.id == other.id


class Author(object):
    def __init__(self, id, name):
        self.id = id
        self.name = name


class BookContainerModel(object):
    implements(IBookContainer)

    def __init__(self, request):
        self._request = request

    def __getitem__(self, key):
        books = get_library(self._request).get_data_as_dict(ids=[int(key)])
        book = books[0]
        book['authors'] = [Author('%i-%i' % (x, book['id'], ), author)
                           for x, author in enumerate(book['authors'])]
        return BookModel(self._request, self, **book)

    def __delitem__(self, key):
        get_library(self._request).delete_book(key)

    def __iter__(self):
        library = get_library(self._request)
        for book in library.get_data_as_dict():
            authors = [Author('%i-%i' % (x, book['id'], ), author)
                       for x, author in enumerate(book['authors'])]
            available_formats = book.get('available_formats', None)
            yield BookModel(request=self._request,
                            parent=self,
                            id=book['id'],
                            title=book['title'],
                            authors=authors,
                            available_formats=available_formats)

    def add(self, title, authors=None):
        m = Metadata(title)
        if authors:
            m.authors = authors
        library = get_library(self._request)
        id = library.create_book_entry(m)
        return self[id]

    def update(self, id, title, authors):
        id = int(id)
        library = get_library(self._request)
        library.set_title(id, title)
        library.set_authors(id, authors)
        return self[id]


class FileModel(object):
    def __init__(self, book_id, id, name):
        self.book_id = book_id
        self.id = id
        self.name = name


class FileContainerModel(object):
    def __init__(self, request):
        self._request = request

    def add(self, book_id, data, name):
        book_id = int(book_id)
        library = get_library(self._request)
        ext = name.split('.')[-1]
        format = get_mimetype(ext)
        library.add_format(book_id, format, data, index_is_id=True)
        return FileModel(book_id=book_id, id='%s_%s' % (str(book_id), format),
                         name=name)


class AuthorContainerModel(object):
    def __init__(self, request):
        self._request = request


class BookStorage(object):
    implements(IBookStorage)

    setting_options = [
        ('calibre.library', 'Configure the path to the calibre library', ''),
    ]

    def init(self, settings):
        self.settings = settings

    get_book_container = BookContainerModel
    get_file_container = FileContainerModel
    get_author_container = AuthorContainerModel

    def get_data_iter(self, request, download):
        book = request.root.c['books'][int(download.__parent__.id)]
        try:
            idx = book.available_formats.index(download.id)
        except ValueError:
            raise KeyError(download.id)
        return open(book.formats[idx], 'rb')

    def get_file_info(self, request, download):
        book = request.root.c['books'][int(download.__parent__.id)]
        idx = book.available_formats.index(download.id)
        return get_file_info(book, book.formats[idx])
