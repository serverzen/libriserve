import os.path
import logging

from zope.interface import implements

from ..interfaces import IAuthor, IBook, IBookContainer, IBookStorage

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import LargeBinary
from sqlalchemy import String
from sqlalchemy import Table
from sqlalchemy import Unicode
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import NoResultFound

from ..utils import get_mimetype
from ..dbsetup import DEFAULT_SQL_URL

logger = logging.getLogger('libriserve')

Base = declarative_base()

bookauthors_table = Table(
    'libriserve_bookauthors', Base.metadata,
    Column('author_id', Integer, ForeignKey('libriserve_authors.id')),
    Column('book_id', Integer, ForeignKey('libriserve_books.id')),
)


class Author(Base):
    implements(IAuthor)

    __tablename__ = 'libriserve_authors'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))

    def __init__(self, name):
        self.name = name


class Book(Base):
    implements(IBook)

    __tablename__ = 'libriserve_books'
    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255))
    authors = relationship('Author', secondary=bookauthors_table)

    def __init__(self, title):
        self.title = title

    def wrap(self, parent):
        self.__parent__ = parent
        return self


class File(Base):
    __tablename__ = 'libriserve_bookfiles'

    id = Column(Integer, primary_key=True)
    data = Column(LargeBinary)
    name = Column(Unicode(255), nullable=False)
    mimetype = Column(String(30), nullable=False)
    book_id = Column(Integer, ForeignKey(Book.id), nullable=False)

    book = relationship('Book', backref=backref('files', cascade='all,delete'))


class BookContainer(object):
    implements(IBookContainer)

    def __init__(self, request):
        self._request = request

    def __getitem__(self, key):
        try:
            return self._request.db.query(Book) \
                .filter(Book.id == key).one().wrap(self)
        except NoResultFound:
            raise KeyError(key)

    def __delitem__(self, key):
        db = self._request.db
        db.query(File).filter(File.book_id == key).delete()
        book = self[key]
        book.authors = []
        db.query(Book).filter(Book.id == key).delete()

        db.commit()

    def __iter__(self):
        return (y.wrap(self)
                for y in self._request.db.query(Book).order_by(Book.title))

    def add(self, title, authors=None):
        book = Book(title=title)
        self._update(book, title=title, authors=authors)
        return book

    def _update(self, book, title, authors=None):
        db = self._request.db

        book.title = title
        book.authors = []

        authors = authors or []
        for x in authors:
            if IAuthor.providedBy(x):
                author = self._request.db.query(Author) \
                    .filter(Author.id == x.id).one()
            else:
                author = Author(x)
                db.add(author)
            book.authors.append(author)

        db.add(book)
        db.commit()
        return book

    def update(self, id, title, authors=None):
        book = self[id]
        self._update(book, title, authors)
        return book


class AuthorContainer(object):

    def __init__(self, request):
        self._request = request

    def __getitem__(self, key):
        return self._request.db.query(Author) \
            .filter(Author.id == key).one()

    def __iter__(self):
        return (y for y in self._request.db.query(Author))


class FileContainer(object):
    def __init__(self, request):
        self._request = request

    def __getitem__(self, key):
        db = self._request.db
        try:
            return db.query(File).filter(File.id == key).one()
        except NoResultFound:
            raise KeyError(key)

    def add(self, book_id, name, data, mimetype=None):
        if mimetype is None:
            ext = '.'.split(name)[-1]
            mimetype = get_mimetype(ext)

        # TODO: fix loading binary data into memory
        bookfile = File(data=data.read(),
                        name=os.path.basename(name),
                        mimetype=get_mimetype(ext),
                        book_id=book_id)
        db = self._request.db
        db.add(bookfile)
        db.commit()

        return bookfile


class BookStorage(object):
    implements(IBookStorage)

    setting_options = [
        ('sqlalchemy.url', 'Configure the database url', DEFAULT_SQL_URL),
    ]

    def init(self, settings):
        self.settings = settings

    def get_book_container(self, request):
        return BookContainer(request)

    def get_author_container(self, request):
        return AuthorContainer(request)

    def get_file_container(self, request):
        return FileContainer(request)

    def get_data_iter(self, request, download):
        try:
            bookfile = request.db.query(File).filter(
                File.id == download.id).one()
        except NoResultFound:
            raise KeyError(download.id)

        return bookfile.data

    def get_file_info(self, request, download):
        return request.db.query(File).filter(
            File.id == download.id).one()
