import sys
import os
import uuid
import base64
import json
import logging

from zope.interface import implements

from ..interfaces import IBook, IBookContainer, IBookStorage

logger = logging.getLogger('libriserve')


def _load(request):
    filename = request.registry.settings['libriserve.jsonfile']
    if not os.path.exists(filename):
        return {'books': {}, 'authors': {}, 'files': {}}

    with open(filename) as f:
        base = json.loads(f.read())
        return base


def _save(request, base):
    filename = request.registry.settings['libriserve.jsonfile']
    with open(filename, 'w') as f:
        f.write(json.dumps(base, indent=2, separators=(', ', ': ')))
    return base


class File(object):
    def __init__(self, base, id, book_id, name='', data='',
                 mimetype='application/octet-stream'):
        self.id = id
        self.book_id = book_id
        self.mimetype = mimetype
        self.name = name
        self.data = data


class Book(object):
    implements(IBook)

    def __init__(self, base, id, title, authors=None, files=None):
        self._base = base

        self.id = id
        self.title = title
        self.authors = [Author(**x) for x in authors or []]
        self.files = [File(base, **base['files'][x]) for x in files or []]

    def __eq__(self, other):
        return self.id == other.id


class Author(object):
    def __init__(self, base, id, name):
        self.id = id
        self.nae = name

    def __eq__(self, other):
        return self.id == other.id


class ItemContainerBase(object):
    base_name = None
    item_factory = None

    def __init__(self, request):
        self._request = request

    def __getitem__(self, key):
        base = _load(self._request)
        item = base[self.base_name][key]
        return self.item_factory(base, **item)

    def __delitem__(self, key):
        base = _load(self._request)
        del base[self.base_name][key]
        _save(self._request, base)

    def __iter__(self):
        base = _load(self._request)
        return (self.item_factory(base, **x)
                for x in base[self.base_name].itervalues())

    def update(self, id, **kwargs):
        base = _load(self._request)
        item = base[self.base_name][id]
        item.update(kwargs)
        self._massage(base, item)
        _save(self._request, base)
        return self.item_factory(base, **base[self.base_name][id])

    def add(self, **kwargs):
        base = _load(self._request)
        item_base = base[self.base_name]

        id = str(uuid.uuid4())
        item = {'id': id}
        item.update(kwargs)
        self._massage(base, item)
        item_base[id] = item
        _save(self._request, base)
        return self.item_factory(base, **item)

    def _massage(self, base, item):
        pass


class AuthorContainer(ItemContainerBase):
    base_name = 'authors'
    item_factory = Author


class FileContainer(ItemContainerBase):
    base_name = 'files'
    item_factory = File

    def _massage(self, base, item):
        item['data'] = base64.encodestring(item['data'].read())
        book = base['books'][item['book_id']]
        if 'files' not in book:
            book['files'] = []
        book['files'].append(item['id'])


class BookContainer(ItemContainerBase):
    implements(IBookContainer)

    base_name = 'books'

    item_factory = Book


DEFAULT_PATH = 'libriserve.json'
_etcdir = os.path.join(sys.prefix, 'etc')
if os.path.isdir(_etcdir):
    DEFAULT_PATH = '%s/libriserve.json' % _etcdir


class BookStorage(object):
    implements(IBookStorage)

    setting_options = [
        ('libriserve.jsonfile', 'Configure the json file', DEFAULT_PATH),
    ]

    def init(self, settings):
        self.settings = settings
        logger.info('JSON file: %s' % settings['libriserve.jsonfile'])

    get_book_container = BookContainer
    get_author_container = AuthorContainer
    get_file_container = FileContainer

    def _get_file(self, request, download):
        book_id = download.__parent__.id
        books = request.root.c['books']
        book = books[book_id]
        for x in book.files:
            if x.id == download.id:
                return x
        raise KeyError(download.id)

    def get_data_iter(self, request, download):
        f = self._get_file(request, download)
        return base64.decodestring(f.data)

    def get_file_info(self, request, download):
        return self._get_file(request, download)
