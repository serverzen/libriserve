from zope.interface.verify import verifyClass
from collections import OrderedDict
import pkg_resources

from ..interfaces import IBookStorage


class StorageInfo(object):
    def __init__(self, name, factory, dist):
        self.name = name
        self.factory = factory
        self.dist = dist

    @classmethod
    def from_entry_point(cls, ep):
        class_ = ep.load()
        verifyClass(IBookStorage, class_)
        return cls(ep.name, class_, ep.dist)

STORAGES = [StorageInfo.from_entry_point(x)
            for x in pkg_resources.iter_entry_points(
                group='libriserve.storages')]

STORAGE_GROUPS = OrderedDict()
for x in STORAGES:
    group = STORAGE_GROUPS.get(x.dist.project_name, None)
    if group is None:
        STORAGE_GROUPS[x.dist.project_name] = group = []
    group.append(x)

STORAGE_MAP = OrderedDict()
for x in STORAGES:
    STORAGE_MAP['%s/%s' % (x.dist.project_name, x.name)] = x
