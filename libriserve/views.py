import os.path
import json

from pyramid.i18n import TranslationStringFactory
from pyramid.view import view_config
from pyramid.response import Response
from pyramid.renderers import render_to_response
from pyramid.security import has_permission

from . import opds
from .utils import get_format
from .interfaces import IBookContainer

_ = TranslationStringFactory('libriserve')


@view_config(context='libriserve.models.RootModel',
             renderer="libriserve:templates/book-listing.jinja2",
             permission='view')
def root(request):
    def check(perm, request=request):
        return has_permission(perm, request.context, request)
    return {'has_permission': check}


@view_config(context='libriserve.models.RootModel',
             name='catalog.atom',
             permission='view')
def opds_feed(request):
    response = Response(content_type='application/atom+xml',
                        body=opds.get_opds_feed(request))
    return response


def get_download_url(request, bookfile):
    return '%s/book_download/%s/%s' % (request.application_url,
                                       str(bookfile.book_id),
                                       str(bookfile.id))


def get_bookfile_format(bookfile):
    return get_format(bookfile.name, bookfile.mimetype)


def book_as_dict(request, book):
    d = {'title': book.title, 'id': book.id}
    d['authors'] = [{'id': x.id, 'name': x.name} for x in book.authors or []]
    d['downloads'] = [{'format': get_bookfile_format(x),
                       'url': get_download_url(request, x)}
                      for x in book.files]
    d['formats'] = [get_bookfile_format(x) for x in book.files]
    return d


@view_config(context='libriserve.interfaces.IBookContainer',
             renderer='json',
             request_method='GET',
             permission='view')
def books(request):
    return {'books': [book_as_dict(request, x) for x in request.context]}


def _author(request, x):
    if isinstance(x, dict):
        return request.root.c.authors[x['id']]
    return x


@view_config(context='libriserve.interfaces.IBookContainer',
             renderer='json',
             request_method='POST',
             permission='add_book')
def add_book(request):
    data = request.json_body
    authors = [_author(request, x) for x in data.get('authors', [])]
    m = request.context.add(title=data['title'], authors=authors)
    return book_as_dict(request, m)


@view_config(context='libriserve.interfaces.IBook',
             request_method='GET',
             renderer='libriserve:templates/book-details.jinja2',
             permission='view')
def book(request, context=None):
    return {'book': json.dumps(book_as_dict(request, request.context))}


@view_config(context='libriserve.interfaces.IBook',
             request_method='PUT',
             renderer='json',
             permission='modify_book')
def save_book(request, context=None):
    data = request.json_body
    res = request.root.c['books'].update(
        id=data['id'],
        title=data['title'],
        authors=data['authors'],
    )
    return book_as_dict(request, res)


@view_config(context='libriserve.interfaces.IBook',
             request_method='GET',
             xhr=True,
             renderer='json',
             permission='view')
def book_json(request, context=None):
    context = context or request.context
    return book_as_dict(request, context)


@view_config(context='libriserve.interfaces.IBook',
             request_method='DELETE',
             renderer='json',
             permission='delete_book')
def delete_book(request, context=None):
    context = context or request.context
    del request.root.c.books[context.id]
    return {'success': True}


@view_config(context='libriserve.models.ImageModel',
             permission='view')
def image(request):
    book = request.context.__parent__.__parent__['books'][request.context.id]
    response = Response(content_type='image/jpeg')
    response.app_iter = open(book_as_dict(request, book)['cover'], 'rb')
    return response


@view_config(context='libriserve.models.DownloadModel',
             permission='view')
def download(request):
    download = request.context
    response = Response(content_type='application/octet-stream')
    storage = request.registry.settings['libriserve.storage']
    response.app_iter = storage.get_data_iter(request, download)
    info = storage.get_file_info(request, download)
    response.content_type = str(info.mimetype)
    response.content_disposition = \
        'attachment; filename="%s"' % info.name
    return response


@view_config(context='libriserve.interfaces.IFileContainer',
             renderer='libriserve:templates/finish-upload.jinja2',
             name='upload',
             permission='add_book_file')
def upload(request):
    fileobj = request.POST.get('file', None)
    book_id = request.POST.get('book_id', None)
    res = {'success': True}
    if fileobj is None or fileobj == '':
        res['success'] = False
        res['message'] = 'Missing file field'
    elif book_id is None or book_id == '':
        res['success'] = False
        res['message'] = 'Missing book_id'
    else:
        files = request.root.c.files
        filemodel = files.add(book_id=book_id,
                              name=os.path.basename(fileobj.filename),
                              data=fileobj.file)
        book = request.root.c.books[book_id]
        res.update({
            'book': book_as_dict(request, book),
            'book_id': book.id,
            'file_id': filemodel.id,
        })

    return {'result': json.dumps(res)}


@view_config(context='pyramid.exceptions.Forbidden')
def forbidden(request):
    response = render_to_response('libriserve:templates/403.jinja2', {},
                                  request=request)
    response.status_int = 403
    return response


@view_config(context='pyramid.exceptions.NotFound')
def notfound(request):
    data = {'message': 'page at %s' % request.path}
    if IBookContainer.providedBy(request.context):
        data = {'message': 'book with id %s' % request.path.split('/')[-1]}
    response = render_to_response('libriserve:templates/404.jinja2',
                                  data,
                                  request=request)
    response.status_int = 404
    return response
