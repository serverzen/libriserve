import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.rst')).read()

requires = [
    'pyramid>=1.4',
    'jinja2>=2.6',
    'pyramid_jinja2>=1.5',
    'sqlalchemy>=0.8b2',
    'webtest>=1.4',
    'alembic>=0.4',
    'ziggurat_foundations==0.3',
    'PyYAML>=3.10',
]

setup(name='LibriServe',
      version='0.1',
      description='Book content server',
      long_description=README+'\n\n'+CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pylons",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="libriserve",
      entry_points={
          'paste.app_factory': [
              'app = libriserve:app',
          ],
          'console_scripts': [
              'libriserve = libriserve.server:console',
              'libriserve-manage = libriserve.manage:main'
          ],
          'libriserve.storages': [
              'calibre = libriserve.storage.calibrestorage:BookStorage',
              'sql = libriserve.storage.sqlstorage:BookStorage',
              'json = libriserve.storage.jsonstorage:BookStorage',
          ],
      },
      )
