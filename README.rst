================================
LibriServe - Book Content Server
================================

Introduction
============

*LibriServe* is a book content server that serves book and book metadata.

Installation
============

*NOTE: LibriServe has only been tested on Ubuntu but should work
on any Python-enabled OS*

It is highly recommended to use the wonderful
`virtualenv <http://www.virtualenv.org>`_ tool to create an isolated
environment for *LibriServe*.  Please see the *virtualenv* docs for
further info on setting that up.

Once the *virtualenv* is activated it is recommended to install
*LibriServe* using *pip*.

From PyPi (latest released version)::

  python virtualenv.py LibriServe
  source LibriServe/bin/activate
  pip install LibriServe

From latest git::

  python virtualenv.py LibriServe
  source LibriServe/bin/activate
  pip install -e git+https://bitbucket.org/serverzen/libriserve.git#egg=LibriServe

Configuration
=============

To get a fresh working system using the sql storage issue the following command::

  libriserve-manage init

This will create a default configuration file at ``$PREFIX/etc/libriserve.yaml``
and an empty (configured) sqlite database at ``$PREFIX/etc/libriserve.db``.


Usage
=====

*LibriServe* is meant to run as a server application.

Command-line
------------

::

  usage: libriserve [-h] [--host HOST] [--port PORT] [--storage BOOK_STORAGE]
                    [--storage-settings STORAGE_SETTINGS]
  
  Run the content server
  
  optional arguments:
    -h, --help            show this help message and exit
    --host HOST           Host/IP to listen on [0.0.0.0 means all interfaces] (default: 0.0.0.0)
    --port PORT           Port to listen on (default: 8080)
    --storage BOOK_STORAGE
                          Storage type to use
    --storage-settings STORAGE_SETTINGS
                          Configuration settings for storage; settings need to be of
                          foo=bar,abc=def format (default: None)

Book Storages
=============

*LibriServe* contains support for three different book storage models.

  1. sql - allows for the storage to live in any SQLAlchemy supported database, please see
     `SQLAlchemy engine configuration`_ for example sql url configuration.  This is the default
     storage with the sql url as sqlite:////path/to/venv/etc/libriserve.db

  2. calibre - stores all content inside a Calibre_ system

  3. json - all content is stored in a flat text file in JSON format.  This is mostly
     a test storage and should not be used in serious environments.


.. _`SQLAlchemy engine configuration`: http://docs.sqlalchemy.org/en/rel_0_8/core/engines.html#sqlalchemy.create_engine
.. _Calibre: http://calibre-ebook.com/


Technology Stack
================

The backend application (written in Python/Pyramid) mostly just exposes
a JSON_ based Restful_ api which the frontend (javascript/jquery/etc) uses
to provide the actual application.

.. _JSON: http://en.wikipedia.org/wiki/JSON
.. _Restful: http://en.wikipedia.org/wiki/Representational_state_transfer

Backend
-------

  * `Python 2.7+ <http://www.python.org/>`_ - Language/platform
  * `Pyramid 1.4+ <http://www.pylonsproject.org/>`_ - Web framework
  * `Jinja2 2.6+ <http://jinja.pocoo.org/>`_ - Django-style template language

Frontend
--------

  * `jQuery 1.9+ <http://jquery.com/>`_
  * `RequireJS 2.1+ <http://requirejs.org/>`_
  * `Knockout 2.2+ <http://knockoutjs.com/>`_
  * `Twitter Bootstrap 2.2+ <http://twitter.github.com/bootstrap/>`_

Running the Tests
=================

Prerequisites
-------------

  * Nose
  * Coverage

Running All Tests
-----------------

::

  nosetests -v libriserve.tests libriserve.integration_tests libriserve.functional_tests

Helpful URL's
=============

  * Source Repository (git) - http://src.serverzen.com/libriserve

Credits
=======

  * Written and maintained by Rocky Burt - rocky AT serverzen DOT com, developer
    at `ServerZen Software <http://www.serverzen.com>`_.
